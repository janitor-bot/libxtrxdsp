Source: libxtrxdsp
Priority: optional
Maintainer: Sepi Gair <sepigair@email.cz>
Build-Depends: debhelper-compat (= 12), cmake
Standards-Version: 4.5.0
Section: libs
Homepage: https://github.com/xtrx-sdr/libxtrxdsp
Vcs-Browser: https://salsa.debian.org/debian-hamradio-team/libxtrxdsp
Vcs-Git: https://salsa.debian.org/debian-hamradio-team/libxtrxdsp.git

Package: libxtrxdsp-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libxtrxdsp0 (= ${binary:Version}), ${misc:Depends}
Description: Library of DSP functions, developed for XTRX SDR: development
 This package is part of the library set for XTRX support.
 .
 XTRX is the smallest easily embeddable software-defined radio (SDR). It is
 both affordable and high-performance. XTRX is designed to enable the next
 generation of wireless solutions, from prototype to production.
 .
 This package contains development files.

Package: libxtrxdsp0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Library of DSP functions, developed for XTRX SDR
 This package is part of the library set for XTRX support.
 .
 XTRX is the smallest easily embeddable software-defined radio (SDR). It is
 both affordable and high-performance. XTRX is designed to enable the next
 generation of wireless solutions, from prototype to production.
 .
 This package contains the shared library and utilities for testing it.
